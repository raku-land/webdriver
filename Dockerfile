FROM alpine:3.20

RUN apk add --no-cache curl firefox \
 && curl -L https://github.com/mozilla/geckodriver/releases/download/v0.34.0/geckodriver-v0.34.0-linux64.tar.gz \
  | tar xz -C /usr/local/bin        \
 && apk del --no-cache curl

CMD ["geckodriver", "--allow-hosts", "geckodriver", "--host", "0.0.0.0"]
