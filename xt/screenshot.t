use Test;
use WebDriver;

constant png-sig = Blob.new: 0x89, |"PNG\r\n".encode, 0x1A, 0x0A;

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

$wd.get: 'http://httpd';

is $wd.screenshot.subbuf(^8), png-sig, 'window screenshot';

is $wd.screenshot-full.subbuf(^8), png-sig, 'window screenshot-full';

is $wd.active-element.screenshot.subbuf(^8), png-sig, 'element screenshot';

done-testing;
