use Test;
use WebDriver;

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

$wd.get('http://httpd');

my $shadow = $wd.find('video').shadow-root;

isa-ok $shadow, 'WebDriver::ShadowRoot';

is $shadow.find('progress').tag, 'progress', 'find';

is $shadow.find-all('progress')[0].tag, 'progress', 'find-all';

done-testing;
