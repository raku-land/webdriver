use Test;
use WebDriver;

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

is-deeply $wd.timeouts, { :implicit(0), :pageLoad(300000), :script(30000) },
    'timeouts';

is-deeply $wd.timeouts( :123script ).timeouts,
    { :implicit(0), :pageLoad(300000), :script(123) }, 'timeouts :123script';

done-testing;
