use Test;
use WebDriver;

constant $url = 'http://httpd/';

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

$wd.get: $url;

$wd.cookie: 'foo', 'bar';
$wd.cookie: 'baz', 'qux', :httpOnly, :sameSite<Strict>, :secure;

my %foo = (
    :domain<httpd>, :!httpOnly, :name<foo>, :path</>, :sameSite<None>,
    :!secure, :value<bar>,
);

my %baz = (
    :domain<httpd>, :httpOnly, :name<baz>, :path</>, :sameSite<Strict>,
    :secure, :value<qux>,
);

is-deeply $wd.cookie, [%foo, %baz], 'cookie';
is-deeply $wd.cookie('baz'), %baz,  'cookie "baz"';

$wd.delete-cookie: 'foo';

is-deeply $wd.cookie, [%baz,], 'cookie (after delete)';

$wd.delete-cookie;

is-deeply $wd.cookie, [], 'cookie (after delete all)';

done-testing;
