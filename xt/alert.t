use Test;
use WebDriver;

my $wd = WebDriver.new: :4444port, :host<geckodriver>,
    :capabilities(:alwaysMatch('moz:firefoxOptions' => :args('-headless',)));

$wd.get('http://httpd');
$wd.find('alert', :using(WebDriver::Selector::LinkText)).click;

is $wd.alert-text, 'hi', 'alert-text';

$wd.alert-dismiss;

done-testing;
