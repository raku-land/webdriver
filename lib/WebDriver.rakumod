unit class WebDriver;

has %.capabilities;

my enum method <DELETE GET POST>;

enum Selector is export (
    CSS             => 'css selector',
    LinkText        => 'link text',
    PartialLinkText => 'partial link text',
    TagName         => 'tag name',
    XPath           => 'xpath',
);

# Forward declare so the base role can reference them.
my class Element    { ... }
my class ShadowRoot { ... }

my role Base {
    need HTTP::Tiny;

    has $!base is built;

    method find(Str:D $value, Selector:D :$using = CSS) of Element {
        self!inflate: self!req: POST, 'element', :$using, :$value;
    }

    method find-all(Str:D $value, Selector:D :$using = CSS) {
        self!req(POST, 'elements', :$using, :$value).map: { self!inflate: $_ };
    }

    method !inflate($pair) {
        temp $!base .= subst: / ( '/element/' | '/shadow/' ) .* /;

        if my $id = $pair<element-6066-11e4-a52e-4f735466cecf> {
            return Element.new: :base("$!base/element/$id");
        }

        if $id = $pair<shadow-6066-11e4-a52e-4f735466cecf> {
            return ShadowRoot.new: :base("$!base/shadow/$id");
        }

        die "Unknown: $pair";
    }

    method !req(method $method, $path = '', *%content) {
        state $ua = HTTP::Tiny.new:
            :default-headers(:content-type<application/json>),
            :agent("WebDriver/$?DISTRIBUTION.meta()<ver> Raku");

        my %res = $ua.request: ~$method, $.defined ?? "$!base/$path" !! $path,
            :content(Rakudo::Internals::JSON.to-json: %content);

        my $value = try {
            CATCH { die %res<content>.decode }

            Rakudo::Internals::JSON.from-json( %res<content>.decode ).<value>;
        }

        die "$_: $value<message>" with $value<error>;

        $value;
    }

    # b64 is a wrapper around req that base64 decodes the result.
    method !b64(|c) {
        state &base64-decode = do {
            try require ::('MIME::Base64');
            ::('MIME::Base64') !~~ Failure ?? { ::('MIME::Base64').decode: $^encoded }
            !! {
                my constant %dec = ( 'A'…'Z', 'a'…'z', 0…9, '+', '/' ).antipairs;
                Blob.new: $^encoded.subst(/ '='+ $/).comb\
                    .map({ %dec{$_}.fmt: '%06b' }).join.comb(8)».parse-base(2)
            }
        }

        return base64-decode self!req: |c;
    }
}
also does Base;

my class Element does Base {
    method enabled  of Str      { self!req: GET,  'enabled'       }
    method clear    of ::?CLASS { self!req: POST, 'clear'; self   }
    method click    of ::?CLASS { self!req: POST, 'click'; self   }
    method label    of Str      { self!req: GET,  'computedlabel' }
    method rect     of Map      { self!req: GET,  'rect'          }
    method role     of Str      { self!req: GET,  'computedrole'  }
    method selected of Bool     { self!req: GET,  'selected'      }
    method tag      of Str      { self!req: GET,  'name'          }
    method text     of Str      { self!req: GET,  'text'          }
    method visible  of Bool     { self!req: GET,  'displayed'     }

    method attr(Str:D $name) of Str { self!req: GET, "attribute/$name" }
    method  css(Str:D $name) of Str { self!req: GET,       "css/$name" }
    method prop(Str:D $name) of Str { self!req: GET,  "property/$name" }

    method send-keys(Str:D $text) of ::?CLASS {
        self!req: POST, 'value', :$text;
        self;
    }

    method screenshot of Blob { self!b64: GET, 'screenshot' }

    method shadow-root of ShadowRoot { self!inflate: self!req: GET, 'shadow' }
}

my class ShadowRoot does Base {}

my SetHash[Str:D] $sessions;

submethod BUILD(:$host = '127.0.0.1', :$port, :%capabilities) {
    $!base = "http://$host:$port/session";

    my %res = self!req: POST, '', :%capabilities;

    %!capabilities = %res<capabilities>;

    $sessions{ $!base ~= "/%res<sessionId>" }++;
}

method alert-accept           { self!req(POST,   'alert/accept');      self }
method alert-dismiss          { self!req(POST,   'alert/dismiss');     self }
method back                   { self!req(POST,   'back');              self }
method forward                { self!req(POST,   'forward');           self }
method get($url)              { self!req(POST,   'url', :$url);        self }
method print(*%args)          { self!b64(POST,   'print', |%args)           }
method refresh                { self!req(POST,   'refresh');           self }
method screenshot             { self!b64(GET,    'screenshot')              }
method source                 { self!req(GET,    'source')                  }
method title                  { self!req(GET,    'title')                   }
method url                    { self!req(GET,    'url')                     }
method window-close           { self!req(DELETE, 'window');            self }
method window-fullscreen      { self!req(POST,   'window/fullscreen'); self }
method window-handle          { self!req(GET,    'window')                  }
method window-handles         { self!req(GET,    'window/handles')          }
method window-maximize        { self!req(POST,   'window/maximize');   self }
method window-minimize        { self!req(POST,   'window/minimize');   self }
method window-new             { self!req(POST,   'window/new')<handle>      }
method window-switch($handle) { self!req(POST,   'window', :$handle);  self }

method active-element of Element {
    self!inflate: self!req: GET, 'element/active'
}

multi method alert-text of Str:D { self!req: GET, 'alert/text' }
multi method alert-text(Str:D $text) of ::?CLASS {
    self!req: POST, 'alert/text', :$text;
    self;
}

multi method cookie              { self!req: GET, 'cookie' }
multi method cookie(Str:D $name) { self!req: GET, "cookie/$name" }
multi method cookie(Str:D $name, Str:D $value, *%args) {
    self!req: POST, 'cookie', cookie => { :$name, :$value, |%args };
}

multi method delete-cookie        { self!req: DELETE, 'cookie';       self }
multi method delete-cookie($name) { self!req: DELETE, "cookie/$name"; self }

method js(Str:D $script, *@args, Bool :$async) {
    self!req: POST, "execute/{ 'a' if $async }sync", :$script, :@args;
}

# Non-standard, URLs vary by browser.
method screenshot-full {
    self!b64: GET, 'moz/' x ($.capabilities<browserName> eq 'firefox') ~
        'screenshot/full';
}

method status of Map {
    # /status is the only path without the session prefix, so suppress it.
    temp $!base .= subst: / '/session/' .* /;

    self!req: GET, 'status';
}

multi method timeouts of Map { self!req: GET, 'timeouts' }
multi method timeouts(*%args where ?%args) of ::?CLASS {
    self!req: POST, 'timeouts', |%args;
    self;
}

multi method window-rect of Map { self!req: GET, 'window/rect' }
multi method window-rect(*%args where ?%args) of ::?CLASS {
    self!req: POST, 'window/rect', |%args;
    self;
}

method delete-session { try { self!req: DELETE; $sessions{$!base}-- } }
method        DESTROY { $.delete-session }
                  END { $?CLASS!req: DELETE, $_ for $sessions.keys }
